Repositórios oficiais do programa "SpaceFM"

http://ignorantguru.github.io/spacefm/
e
https://app.transifex.com/ignorantguru/spacefm/language/pt_BR/

Traduções revisadas por marcelocripe:

https://gitlab.com/marcelocripe/SpaceFM_pt_BR/-/blob/main/for_use_spacefm_spacefmpot_pt_BR_12-02-2022.po?ref_type=heads



Para utilizar o arquivo "for_use_spacefm_spacefmpot_pt_BR_12-02-2022.po", inicie o Emulador de Terminal na pasta onde estão os arquivos que foram baixados e aplique os comandos.

Comando para converter o arquivo editável da tradução com a extensão ".po" para ".mo".

$ msgfmt for_use_spacefm_spacefmpot_pt_BR_12-02-2022.po -o spacefm.mo

Comando para copiar o arquivo da tradução com a extensão ".mo" para a pasta do idioma "pt_BR".

$ sudo cp spacefm.mo /usr/share/locale/pt_BR/LC_MESSAGES
